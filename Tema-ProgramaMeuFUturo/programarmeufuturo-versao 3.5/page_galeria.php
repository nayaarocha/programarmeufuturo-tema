<?php
/*
Template Name: Page_galeria
*/
  
?>


<?php get_header() ?>

	<section id="container">
		<section class="title_bg">
			<section class="centraliza">
				<h1><?php the_title() ?></h1>
			</section>		
		</section>
		
		<section id="content">
			<!-- .post -->
			<?php the_post() ?>
			<section id="post-<?php the_ID() ?>" class="<?php sandbox_post_class() ?>">
				
				<section class="entry-content">
					<?php the_content() ?>
				<div class="fb-comments" data-href="<?php the_permalink() ?>" data-num-posts="5" data-width="1000" data-colorscheme="light" data-mobile="false"></div>
				</section>
			
			</section>


<?php if ( get_post_custom_values('comments') ) comments_template() // Add a key+value of "comments" to enable comments on this page ?>

		</section><!-- #content -->
	</section><!-- #container -->
		
<?php get_footer() ?>