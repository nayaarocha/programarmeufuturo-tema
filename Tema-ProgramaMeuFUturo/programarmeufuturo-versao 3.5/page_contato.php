<?php
/*
Template Name: Page_contato
*/
   
    function h($str) {
    return htmlentities($str);
    }
     
    function noempty($str) {
    if (preg_match('/[a-z]/', $str))
    return true;
    else
    return false;
    }
     
    if (isset($_POST['enviar'])) {
    if (!noempty($_POST['nome']) or !noempty($_POST['assunto']) or !is_email($_POST['email']) or !noempty($_POST['msg'])) {
    $_SESSION['info'] = 'Preencha todos campos corretamente.';
    }
    else {
    $headers = 'From: ' . $_POST['email'] . "\r\n" .
    'Reply-To: ' . $_POST['email'] . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
     
    if(@mail(get_bloginfo('admin_email'), $_POST['assunto'], $_POST['msg'], $headers)) {
    $_SESSION['info'] = 'E-mail enviado com sucesso.';
    header('Location: http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
    exit;
    } else {
    $_SESSION['info'] = 'Erro no servidor.';
    }
    }
    }
    
?>



<?php get_header() ?>

	<section id="container">
		<section class="title_bg">
			<section class="centraliza">
				<h1><?php the_title() ?></h1>
			</section>		
		</section>
		
		<section id="content">
			<!-- .post -->
			<?php the_post() ?>
			<section id="post-<?php the_ID() ?>" class="<?php sandbox_post_class() ?>">
				
				<section class="entry-content">
					<?php the_content() ?>
					    <form method="post" action="<?php bloginfo('template_url') ?>/envia.php" class="contato">
						    < ?php
						    if (isset($_SESSION['info']) {
						    echo '
						    ‘ . $_SESSION['info'] . ";
						    unset($_SESSION['info']);
						    }
						    ?>
						    <div>
						    <label for="nome">* Nome</label>
						    <input type="text" name="nome" value="" id="nome" />
						    </div>
						    <div>
						    <label for="email">* E-mail</label>
						    <input type="text" name="email" value="" id="email" />
						    </div>
						    <div>
						    <label for="assunto">* Assunto</label>
						    <input type="text" name="assunto" value="" id="assunto" />
						    </div>
						    <div>
						    <label for="msg">* Mensagem</label>
						    <textarea name="msg"></textarea>
						    </div>
						    <div>
						    <input type="submit" name="enviar" value="Enviar" />
						    </div>
						</form>
				</section>
			</section>

<?php if ( get_post_custom_values('comments') ) comments_template() // Add a key+value of "comments" to enable comments on this page ?>

		</section><!-- #content -->
	</section><!-- #container -->

<?php get_footer() ?>