<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes() ?>>
<head profile="http://gmpg.org/xfn/11">
	<title><?php wp_title( '-', true, 'right' ); echo wp_specialchars( get_bloginfo('name'), 1 ) ?></title>
	<meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/geral.css" />
   <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/style.css" />
   <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/internas.css" />
	<?php wp_head() // For plugins ?>
  <!-- <script src='http://code.jquery.com/jquery-latest.js'></script -->

   <!-- Menu fixo
   <script src="<?php bloginfo('template_url') ?>/js/jquery-1.6.3.min.js"></script>-->
   <script src="<?php bloginfo('template_url') ?>/js/script.js"></script>

	
</head>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=672744906138904&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<body>
     <section id="barra">
         <section class="centraliza">
            <div class="cnpq">
               <a href="http://www.cnpq.br/" title="Acesso o site do CNPQ" target="_blank">
                  Financiado pelo
                  <img src="<?php bloginfo('template_url');?>/images/logo_cnpq.png">
               </a>

            </div>
         </section>
      </section> <!--Fim section barra-->


   <header>     
      <section class="topo">
         <a class="logo" href="http://www.programarmeufuturo.com/">
            <img src="<?php bloginfo('template_url'); ?>/images/logo.png">
         </a>

         <nav>
           
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) );?>
            <!--<ul>
               <li> <a href="#">Home</a></li>  
               <li><a href="#">Sobre nós</a></li>
               <li><a href="#">Code Girl</a></li>
               <li><a href="#">Galeria</a></li>
               <li> <a href="#">Contato</a></li> 
              

            </ul> -->
         </nav> <!--fim menu-->
      </section> <!-- fim topo-->
   </header> <!--Fim do header--> 

  
