	<footer>
      <section class="centraliza">
         <p> Programar meu futuro © Copyright 2014 - Todos os direitos reservados.</p>

         <ul class="social">
            <li>
               <a href="https://www.facebook.com/programarmeufuturo" target="_blank">
                  <img src="<?php bloginfo('template_url');?>/images/icon_face.png">
               </a>
            <li>

            <li>
               <a href="#">
                  <img src="<?php bloginfo('template_url');?>/images/icon_twitter.png">
               </a>
            <li>

            <li>
               <a href="#">
                  <img src="<?php bloginfo('template_url');?>/images/icon_gmais.png">
               </a>
            <li>

            <li>
               <a href="#">
                  <img src="<?php bloginfo('template_url');?>/images/icon_youtube.png">
               </a>
            <li>

         </ul>
      </section>
   </footer>

<?php wp_footer() ?>

</body>
</html>