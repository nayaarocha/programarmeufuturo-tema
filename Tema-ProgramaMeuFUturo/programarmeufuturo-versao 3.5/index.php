<?php get_header() ?>



	 

   <?php if ( function_exists( 'get_wp_parallax_content_slider' ) ) { get_wp_parallax_content_slider(); } ?>

   <content>

   		<!--Conteudo bloco site-->

   		<section id="conteudo_bloco">		   

               <?php if (have_posts()) : ?>

               

               <?php while (have_posts()) : the_post(); $loopcounter++; ?>

                <a class="bloco" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">

                  <?php if ($loopcounter <= 4) { 

                     the_post_thumbnail(); } ?>

                     <h2><?php the_title(); ?></h2>

                     <p><?php echo(limit_words(get_the_content(),20)); ?></p>

                </a>

           

               <?php endwhile; ?>

               <?php else : ?>

               <?php endif; ?>



            <!--<a class="bloco" href="#">

   				<img src="<?php bloginfo('template_url');?>/images/img01.jpg">

   				<h2>Como começamos</h2>

   				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet tellus id eros dapibus condimentum id eget risus.</p>

   			</a>



   			<a class="bloco" href="#">

   				<img src="<?php bloginfo('template_url');?>/images/img01.jpg">

   				<h2>Filosofia</h2>

   				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet tellus id eros dapibus condimentum id eget risus.</p>

   			</a>



   			<a class="bloco" href="#">

   				<img src="<?php bloginfo('template_url');?>/images/img01.jpg">

   				<h2>Nosso trabalho</h2>

   				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet tellus id eros dapibus condimentum id eget risus.</p>

   			</a>-->

   		</section>



   		<section id="equipe">

   			<section class="centraliza">

   				<img src="<?php bloginfo('template_url');?>/images/icon_equipe.png">

   				<h1>Equipe</h1>

   				

   				<ul class="equipe1">

   					<li>

   						<a>

   							<img src="<?php bloginfo('template_url');?>/images/claudia.png">

   							<span class="name">Claudia Ribeiro</span>

   							<p>Coordenadora</p>

   							

   						</a>

   					</li>



   					<li>

   						<a>

   							<img src="<?php bloginfo('template_url');?>/images/suzy.png">

   							<span class="name">Suzyanne Oliveira</span>

   							<p>Tutora e conteudista</p>

   						</a>

   					</li>



   					<li>

   						<a>

   							<img src="<?php bloginfo('template_url');?>/images/naya.png">

   							<span class="name">Nayara Rocha</span>

   							<p>Web Master e tutora</p>

   							

   						</a>

   					</li>



   					<li>

   						<a>

   							<img src="<?php bloginfo('template_url');?>/images/bartira.png">

   							<span class="name">Bartira Rocha</span>

   							<p>Colaboradora</p>

   						</a>

   					</li>

   				</ul>





   				<ul class="equipe1 equipe2">

   					<li>

   						<a>

   							<img src="<?php bloginfo('template_url');?>/images/jorgiano.png">

   							<span class="name">Jorgiano Vidal</span>

   							<p>Conteudista</p>

   						</a>

   					</li>



   					<li>

   						<a>

   							<img src="<?php bloginfo('template_url');?>/images/lucena.png">

   							<span class="name">Leonardo Lucena</span>

   							<p>Conteudista</p>

   						</a>

   					</li>



   					<li>

   						<a>

   							<img src="<?php bloginfo('template_url');?>/images/soraya.png">

   							<span class="name">Soraya</span>

   							<p>Professora de matemática</p>

   						</a>

   					</li>

   				</ul>

   			</section>

   		</section>



         <section id="parceiros">

            <section class="centraliza">

               <img src="<?php bloginfo('template_url');?>/images/icon_parceiros.png">

               <h1>Parceiros</h1>



               <ul>

                  <li>

                     <a href="http://portal.ifrn.edu.br/" target="_blank"><img src="<?php bloginfo('template_url');?>/images/logo_ifrn.png"></a>

                  </li>



                   <li>

                     <a href="http://www.thoughtworks.com/pt/" target="_blank"><img src="<?php bloginfo('template_url');?>/images/logo_tw.png"></a>

                  </li>



                  <li>

                     <a href="http://www.uern.br/" target="_blank"><img src="<?php bloginfo('template_url');?>/images/logo_uern.png"></a>

                  </li>



                  <li>

                     <a href="https://www.google.com.br/search?q=floriano+cavalcanti&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:pt-BR:official&client=firefox-a&channel=sb&gfe_rd=cr&ei=Xo9vU6mHM6Wj8wfp_YCoBQ#channel=sb&q=escola%20estadual%20floriano%20cavalcanti%20floca&rls=org.mozilla:pt-BR:official" target="_blank"><img src="<?php bloginfo('template_url');?>/images/logo_floca.png"></a>

                  </li>

               </ul>

            </section>

         </section> <!--Fim parceiros-->



         <section id="patrocinadores">

            <section class="centraliza">

              <img src="<?php bloginfo('template_url');?>/images/icon_parceiros.png">

               <h1>Patrocinadores</h1>



               <ul>

                  <li>

                     <a href="http://artkamizetas.com/" target="_blank" ><img src="<?php bloginfo('template_url');?>/images/logo_artkamisetas.png"></a>

                  </li>



		    	  <li>

                     <a href="http://novatec.com.br/" target="_blank" ><img src="<?php bloginfo('template_url');?>/images/logo_novatec.png"></a>

                  </li>
                  
                  <li>
                  	 <a href="https://www.facebook.com/pages/Sweet-Brownie/319245354797509?fref=ts" target="_blank" ><img src="<?php bloginfo('template_url');?>/images/sweet_brownie.png"></a>	
                  </li>

  		 <li>
                  	 <a href="https://www.facebook.com/tribobottons?fref=ts" target="_blank" ><img src="<?php bloginfo('template_url');?>/images/tribo_bottons.png"></a>	
                  </li>

		

               </ul>

            </section>

         </section> <!-- Fim patrocinadores-->



   </content>

<?php get_footer() ?>