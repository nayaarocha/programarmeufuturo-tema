<?php
/*
Template Name: Page_inscricoes
*/
?>
<?php get_header() ?>

    <section id="container">
        <section class="title_bg">
            <section class="centraliza">
                <h1><?php the_title() ?></h1>
            </section>      
        </section>
        
        <section id="content">
            <!-- .post -->
            <?php the_post() ?>
            <section id="post-<?php the_ID() ?>" class="<?php sandbox_post_class() ?>">
                
                <span class="title_inscricoes">Informações Importantes:</span>

                <p><span class="nome">Onde será o evento ? </span><span class="des">IFRN - Natal Central (Mini-auditório)</span> </p>
                </br>
                <p><span class="nome"> Qual será o dia ? </span><span class="des"> 31/05/2014</span></p>
                </br>
                <p><span class="nome"> Que horas começa ? </span> <span class="des">O credenciamento será as 8:00 e o evento começa às 8:30</p><span>

                </br>
                </br>
               



                <span class="title_inscricoes">Palestras</span>

                <ul class="palestrantes">
                    <li>
                        <img src="<?php bloginfo('template_url');?>/images/carol.png">
                             <p class="nome">Caroline Albuquerque Dantas Silva</p> 

                            <p><span class="text_dados">Palestra :</span> Mulheres e Computação</p>
                            
                            
                            <p><span class="text_dados">Minicurrículo:</span>
                           Engenheira de Computação pela Universidade Federal do Rio Grande do Norte, onde
                            realizou várias pesquisas nas áreas de Inteligência Artificial e Otimização de Sistemas.
                            Analista de Sistemas na Activesoft Consultoria.
                            Apoia ações e eventos como o PyLadies Brasil, que visam inspirar e estimular a participação de mulheres em Tecnologia de Informação.</p>
                    </li>



                     <li>
                        <img src="<?php bloginfo('template_url');?>/images/camila.png">
                            <p class="nome"> Camilla Crispim </p> 

                            <p><span class="text_dados">Palestra :</span> O que você precisa saber sobre o fantástico mundo da programação.</p>
                            
                            
                            <p><span class="text_dados">Minicurrículo:</span>
                            Camilla Crispim tem graduação e Mestrado em Ciência da Computação pela Universidade Federal de Campina Grande (UFCG). É Consultora de Desenvolvimento da ThoughtWorks e está sempre envolvida em ações e eventos como o Rails Girls, que visam inspirar mulheres e maximizar a participação feminina na Tecnologia da Informação. Camila acredita que as mulheres têm espaço para obter conquistas na área e que serão as líderes do Brasil de amanhã.
                            </p>
                    </li>


                    <li>
                        <img src="<?php bloginfo('template_url');?>/images/gislene.png">
                            <p class="nome">Gislene Pereira</p> 

                            <p><span class="text_dados">Palestra :</span> O que você precisa saber sobre o fantástico mundo da programação.</p>
                            
                            
                            <p><span class="text_dados">Minicurrículo:</span>
                           Bacharel em Engenharia da Computação, Gislene é baiana mas trabalha na ThoughtWorks Recife. Como Consultora de Desenvolvimento faz parte de um projeto que utiliza Ruby on Rails e está sempre disposta a enfrentar novos desafios.

Em 2013, participou de um concurso do Ministério da Justiça utilizando Dados Abertos e seu time conquistou o 2º lugar nacional. É apaixonada por tecnologia e acredita que com ela podemos construir um mundo melhor e mais justo para todos.

</p>
                    </li>


                    <li>
                        <img src="<?php bloginfo('template_url');?>/images/lilian.png">
                            <p class="nome"> Lilian de Munno </p> 

                            <p><span class="text_dados">Palestra :</span> Desprogramar o preconceito: Mulheres, Tecnologia, Mercado de Trabalho e Você! .</p>
                            
                            
                            <p><span class="text_dados">Minicurrículo:</span>
                            Analista de Negócios Senior da ThoughtWorks, com experiência em projetos com equipes distribuídas, concentra sua atuação na Análise de Negócios Ágil, uma aplicação dos conceitos de análise de negócios em concordância com os princípios do manifesto ágil.
                            Graduada em Relações Internacionais com foco em Economia pela FAAP e em Comunicação Social pela Faculdade Cásper Líbero. 
                            Trabalha há mais de 10 anos com projetos em diferentes campos de aplicação da tecnologia para empresas como Grupo RBS, WebTraining, Instituto de Pesquisas Tecnológicas do estado de SP.</p>
                    </li>



                </ul>

                 <span class="title_inscricoes">Mini-cursos <span class="nome"> (Inscrições para mini-cursos encerradas - aguarde uma nova chamada)</span></span>

                 <ul class="palestrantes cursos">
                    <li>
                         <img src="<?php bloginfo('template_url');?>/images/ruby.png">
                           <p><span class="text_dados">Mini-curso de  Ruby: </span>
                                Mini-curso com 3 horas de duração da linguagem de programação Ruby para iniciantes.
                                 <p><span class="text_dados">Tutores: </span> Leonardo Lucena e alunos do curso de Análise de sistemas - IFRN.</p>
                                <p><span class="text_dados"><a href="http://www.programarmeufuturo.com/mini-curso-ruby/">Visualizar turmas</a></p>
                            </p>
                    </li>
                    
                     <li>
                         <img src="<?php bloginfo('template_url');?>/images/rails.png">
                           <p><span class="text_dados">Mini-curso de  Rails: </span>
                                Mini-curso com 3 horas de duração do framework Rails para quem já conhece um pouco de programação.
                                <p><span class="text_dados">Tutores:</span> Camilla Crispim e Gislene Pereira  </p>
                                <p><span class="text_dados"><a href="http://www.programarmeufuturo.com/wp-content/uploads/2014/05/TURMA-RAILS.pdf" target="_blank">Visualizar turma</a></p>
                            </p>
                    </li>
					
 				</ul> <!--Fim palestrantes cursos-->
                
                 <span class="title_inscricoes">Coffee Code Girl</span>

                 <!-- Coffee code girl-->
                 <ul class="palestrantes">
                 	    <li>
                        <img src="<?php bloginfo('template_url');?>/images/cafe.png">
                            <p class="nome"> O que é o Coffee CODE GIRL ? </p> 

                         
                            
                            
                            <p><span class="text_dados"></span>
                            	COFFEE CODE GIRLS é um momento só para mulheres que compareceram ao CODE GIRL.  Nesse momento vamos bater um papo descontraído e gostaríamos de saber  o que você Girl achou do evento, suas opiniões sobre computação e TI.
Esperamos você às 17:00! 

                            
                            
                            </p>
                    </li>
                 
                 
                 </ul>
                
               	 
                <p class="suzyanne">Importante</p>
                <span class="title_inscricoes title2">Em virtude da grande procura pelo evento, a equipe organizadora precisou encerrar antecipadamente as inscrições para o CODE-GIRL.
				Os espaços reservados para o evento atingiram em 3 dias sua capacidade máxima e  queremos receber nossos convidados muito bem.
				Novos eventos acontecerão ainda este ano.
				Consulte regularmente a página para saber a programação.</br>
				</br>
				Pedimos desculpas pelo inconveniente e agradecemos a compreensão!</br>
				Equipe PROGRAMAR MEU FUTURO</span>
               
                
  <script src="http://www.doity.com.br/js/box_inscricao.js" type="text/javascript"></script>
            <iframe scrolling="0" 
                       id="iframe-inscricao" 
                       onload="setIframeHeight(this.id)" 
                       style="width: 100%;height: 500px;border: none;"
                       src="http://www.doity.com.br/codegirl/passo1">
            </iframe>
            <p style="text-align:center;
                  color:#666672;
                  font-size:14px;
                  font-family:'Open Sans',Helvetica, Arial, sans-serif;
                  font-weight:bolder;">
                Caso n&atilde;o esteja conseguindo fazer a inscri&ccedil;&atilde;o  
                <a target="_blank" 
                    href="http://www.doity.com.br/codegirl">clique aqui 
                </a>
            </p>


                <section class="entry-content">
                    <?php the_content() ?>
                </section>
            </section>

<?php if ( get_post_custom_values('comments') ) comments_template() // Add a key+value of "comments" to enable comments on this page ?>

        </section><!-- #content -->
    </section><!-- #container -->



<?php get_footer() ?>